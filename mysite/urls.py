from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('', include('home.urls')),
    path('japanese/', include('japanese.urls')),
    path('timeline/', include('timeline.urls')),
    path('kitchen_memo/', include('kitchen_memo.urls')),
    path('admin/', admin.site.urls),
]
