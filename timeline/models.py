from django.db import models

ENTRY_TYPE_CHOICES = (
  ('E', 'event'),
  ('T', 'timespan'),
)

class Entry(models.Model):
    entry_type = models.CharField(max_length=1, choices=ENTRY_TYPE_CHOICES)
    label = models.CharField(max_length=2000)
    entry_id = models.CharField(max_length=200)
    left_margin = models.CharField(max_length=10)
    color = models.CharField(max_length=10)
    width = models.CharField(max_length=10)
    start_year = models.IntegerField()
    end_year = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.entry_id

class Tag(models.Model):
    entry = models.ForeignKey(Entry, on_delete=models.CASCADE)
    tag_text = models.CharField(max_length=200)

    def __str__(self):
        return self.tag_text
