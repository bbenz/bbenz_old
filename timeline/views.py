from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

def index(request):
    import json
    json_file = 'static/timeline/timeline.json'
    f = open(json_file, 'r')
    timeline = json.loads(f.read())
    f.close()
    if request.method == 'POST':
        post = request.POST
        new_entry = {
            "entry_type": post['entry_type'],
            "label": post['label'],
            "id": post['id'],
            "left_margin": str(post['position']) + "%",
            "width": "3ch",
            "color": post['color'],
            "date": {}
        }
        if new_entry['entry_type'] == "time_span":
            new_entry['date']['start_date']['year'] = int(post['start_year'])
            new_entry['date']['end_date']['year'] = int(post['end_year'])
        else:
            new_entry['date']['year'] = int(post['start_year'])
        timeline['entries'].append(new_entry)
        fp = open(json_file, 'w')
        json.dump(timeline, fp)
        fp.close()
        return HttpResponseRedirect('/timeline')

    START_YEAR = timeline['start_year']
    START_YEAR = -100
    END_YEAR = timeline['end_year']
    from .models import Entry
    for entry in Entry.objects.all():
        if entry['entry_type'] == "time_span":
            entry['margin_top'] = entry['date']['start_date']['year'] - START_YEAR
            entry['height'] = entry['date']['end_date']['year'] - entry['date']['start_date']['year']
        else:
            entry['margin_top'] = entry['date']['year'] - START_YEAR
            entry['height'] = 1
        entry['color'] = entry['color'] if ('color' in entry) else "var(--color-theme-3)"

    bundle = []
    # create years
    for i in range(START_YEAR, END_YEAR+1):
        year = i
        classes = 'year '
        if i % 10 == 0:
            classes += 'decade '
        if i % 100 == 0:
            classes += 'century '

        # going to use spacify in the template
        formattedYear = str(year).rjust(4, ' ')
        thisYear = {
            'year': year,
            'formatted_year': formattedYear,
            'classes': classes,
        }
        bundle.append(thisYear)

    context = {
        'bundle': bundle,
        'timeline': timeline
    }
    return render(request, 'timeline/index.html', context)

