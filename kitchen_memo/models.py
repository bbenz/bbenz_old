from django.db import models

# Create your models here.

class Recipe(models.Model):
    name = models.CharField(max_length=200)
    link = models.CharField(max_length=2000, blank=True)
    instructions = models.TextField(blank=True)

    def __str__(self):
        return self.name

class Tag(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    tag_text = models.CharField(max_length=200)

    def __str__(self):
        return self.tag_text

class Memo(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    date = models.DateField()
    memo_text = models.TextField()
    rating = models.CharField(max_length=2)

    def __str__(self):
        return str(self.date) + str(self.recipe)
