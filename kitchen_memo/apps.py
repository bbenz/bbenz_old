from django.apps import AppConfig


class KitchenMemoConfig(AppConfig):
    name = 'kitchen_memo'
