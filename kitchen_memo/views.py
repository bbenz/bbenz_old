from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse

default_tags = {
    'meals': [
        {
            'html_name': 'breakfast',
            'display_name': 'Breakfast'
        },
        {
            'html_name': 'lunch',
            'display_name': 'Lunch'
        },
        {
            'html_name': 'dinner',
            'display_name': 'Dinner'
        },

    ],
    'protein': [
        {
            'html_name': 'beef',
            'display_name': 'Beef',
        },
        {
            'html_name': 'salmon',
            'display_name': 'Salmon',
        },
        {
            'html_name': 'pork',
            'display_name': 'Pork',
        },
        {
            'html_name': 'shrimp',
            'display_name': 'Shrimp',
        },
        {
            'html_name': 'chicken',
            'display_name': 'Chicken'
        },
    ]
}

def index(request):
    return render(request, 'kitchen_memo/index.html')

def all(request):
    from kitchen_memo.models import Recipe, Tag
    print(Recipe.objects.all())
    recipes = Recipe.objects.all()
    context = {
        'recipes': Recipe.objects.all(),
    }
    return render(request, 'kitchen_memo/all_recipes.html', context)

def search(request):
    return render(request, 'kitchen_memo/search_recipes.html')

def add(request):
    from kitchen_memo.forms import RecipeForm
    form = RecipeForm()
    if request.method == "POST":
        from kitchen_memo.models import Recipe, Tag
        form = RecipeForm(request.POST)
        if form.is_valid():
            r = Recipe(name=request.POST['name'], link=request.POST['link'], instructions=request.POST['instructions'])
            r.save()
            tag_list = request.POST.getlist('tags[]')
            for tag in request.POST.getlist('tags[]'):
                t = Tag(recipe=r, tag_text=tag)
                t.save()
            redirect_url = '/kitchen_memo/recipe/' + str(r.id)
            return HttpResponseRedirect(redirect_url)
        else:
            print(form.errors)
    context = {
        'form': form
    }

    return render(request, 'kitchen_memo/add_recipe.html', context)

def edit(request, recipe_id):
    from kitchen_memo.models import Recipe
    this_recipe = Recipe.objects.get(id=recipe_id)
    from kitchen_memo.forms import RecipeForm
    form = RecipeForm()
    if request.method == "POST":
        from kitchen_memo.models import Recipe, Tag
        form = RecipeForm(request.POST)
        if form.is_valid():
            this_recipe.name = request.POST['name']
            this_recipe.link = request.POST['link']
            this_recipe.instructions = request.POST['instructions']
            this_recipe.save()
            for tag in request.POST.getlist('tags[]'):
                print('i think were not getting here')
                t = Tag(recipe=this_recipe, tag_text=tag)
                t.save()
            redirect_url = '/kitchen_memo/recipe/' + str(this_recipe.id)
            return HttpResponseRedirect(redirect_url)
    from kitchen_memo.models import Tag
    tags = Tag.objects.filter(recipe=this_recipe)
    checkboxes = {}
    tag_strings = []
    for tag in tags:
        tag_strings.append(tag.tag_text)
        checkboxes[tag.tag_text] = 'checked'
    for section in default_tags:
        for tag in default_tags[section]:
            if tag['html_name'] in tag_strings:
                tag['checked'] = 'checked'

    context = {
        'form': form,
        'recipe': this_recipe,
        'default_tags': default_tags,
        'checkboxes': checkboxes
    }
    return render(request, 'kitchen_memo/edit_recipe.html', context)

def recipe(request, recipe_id):
    from kitchen_memo.models import Recipe
    this_recipe = Recipe.objects.filter(id=recipe_id)[0]
    print(this_recipe)
    from kitchen_memo.models import Tag
    tags = Tag.objects.filter(recipe=this_recipe)
    context = {
        'recipe': this_recipe,
        'tags': tags
    }
    return render(request, 'kitchen_memo/recipe.html', context)

def memo(request, recipe_id):
    return HttpResponse("You're looking at question %s." % recipe_id)
