from django.urls import path
from . import views

app_name = 'kitchen_memo'
urlpatterns = [
    path('', views.index, name='home'),
    path('all/', views.all, name="all_recipes"),
    path('search_recipes/', views.search, name="search_recipes"),
    path('add/', views.add, name="add_recipe"),
    path('edit/<int:recipe_id>/', views.edit, name="edit_recipe"),
    path('recipe/<int:recipe_id>/', views.recipe, name="recipe"),
    path('memo/<int:recipe_id>/', views.memo, name="memo"),
]
