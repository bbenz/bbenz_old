window.addEventListener('load', function () {
  const MIN_ROW_HEIGHT = 1;
  const MAX_ROW_HEIGHT = 100;

  let zoomPlus = document.querySelector('#zoom-plus');
  let zoomText = document.querySelector('#zoom-text');
  let zoomTextBtn = document.querySelector('#zoom-text-btn');
  let zoomMinus = document.querySelector('#zoom-minus');
  let zoomSlider = document.querySelector('#zoom-slider');
  let newEntryButton = document.querySelector('#add-new-entry-btn');

  var entrySlider = false;
  newEntryButton.onclick = function() {
    if (entrySlider) {
      closeSidebar();
      entrySlider = false;
    } else {
      openSidebar();
      entrySlider = true;
    }
  }

  // open the new entry button when testing
  newEntryButton.click();

  let html = document.getElementsByTagName('html')[0];
  let rowHeight = 5;
  zoomSlider.value = rowHeight;
  html.style.setProperty("--row-height", rowHeight + "px");

  zoomPlus.onclick = function(){
    scaledOffset = Math.round(window.pageYOffset / rowHeight);
    rowHeight++;
    newZoomTo = scaledOffset * rowHeight;
    updateWidget();
    document.getElementById(scaledOffset).scrollIntoView({block: 'start', behavior: 'smooth'});
  };

  zoomMinus.onclick = function(){
    rowHeight--;
    html.style.setProperty("--row-height", rowHeight + "px");
    updateWidget();
  };

  zoomSlider.addEventListener("input", function(){
    rowHeight = this.value;
    html.style.setProperty("--row-height", rowHeight + "px");
    updateWidget();
  });

  zoomText.addEventListener("keyup", function(event) {
    if (event.key === "Enter") {
      zoomToId(this.value);
    }
  });

  zoomTextBtn.onclick = zoomToId;

  function zoomToId(){
    zoomTo = zoomText.value;
    zoomToElem = document.getElementById(zoomTo);
    zoomText.value = '';
    windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    elemHeight = zoomToElem.offsetHeight;
    //scrollBlock = (elemHeight > windowHeight) ? 'start' : 'center';
    scrollBlock = 'start';
    document.getElementById(zoomTo).scrollIntoView({block: scrollBlock, behavior: 'smooth'});
    window.setTimeout(function(){ zoomText.focus() }, 1000);
  }

  function updateWidget(){
    html.style.setProperty("--row-height", rowHeight + "px");
    if (rowHeight <= MIN_ROW_HEIGHT) {
      zoomMinus.disabled = true;
    } else {
      zoomMinus.disabled = false;
    }

    if (rowHeight >= MAX_ROW_HEIGHT) {
      zoomPlus.disabled = true;
    } else {
      zoomPlus.disabled = false;
    }
    zoomSlider.value = rowHeight;
    console.log('this is from updateWidget');
    console.log(rowHeight);
  }


});

function openSidebar() {
  sidebar = document.querySelector('#sidebar-controls');
  myapp = document.querySelector('#my-app');
  sidebar.style.width = '250px';
  myapp.style.marginRight = '250px';
}

function closeSidebar() {
  sidebar = document.querySelector('#sidebar-controls');
  myapp = document.querySelector('#my-app');
  sidebar.style.width = '0';
  myapp.style.marginRight = '0';
}
