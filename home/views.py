from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# Create your views here.

def index(request):
    template = loader.get_template('home/index.html')
    return HttpResponse(template.render())

def dice(request):
    template = loader.get_template('home/dice.html')
    return HttpResponse(template.render())

def bash(request):
    template = loader.get_template('home/bash.html')
    return HttpResponse(template.render())

