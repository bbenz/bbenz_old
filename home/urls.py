from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('alternate-diceware/', views.dice, name="whatever"),
    path('bash', views.bash, name="bash"),
]
