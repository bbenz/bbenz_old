from django.db import models

# Create your models here.

class Ending(models.Model):
    # endings will not contain golden ru or golden i, they get cut off before that to stay uniform
    form = models.CharField(max_length=20, primary_key=True)
    a = models.CharField(max_length=20, blank=True)
    i = models.CharField(max_length=20, blank=True)
    u = models.CharField(max_length=20, blank=True)
    e = models.CharField(max_length=20, blank=True)
    o = models.CharField(max_length=20, blank=True)
    te = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return self.form

class Group(models.Model):
    edict_key = models.CharField(max_length=20, primary_key=True)
    edict_explanation = models.CharField(max_length=200)
    classification = models.CharField(max_length=20) # I, II, III
    irregular = models.BooleanField(default=False)

    negative = models.CharField(max_length=20)
    masu = models.CharField(max_length=20, blank=True)
    plain = models.CharField(max_length=20)
    potential = models.CharField(max_length=20, blank=True)
    volitional = models.CharField(max_length=20, blank=True)
    te = models.CharField(max_length=20)
    ta = models.CharField(max_length=20)

    passive = models.CharField(max_length=20, blank=True)
    causative = models.CharField(max_length=20, blank=True)
    conditional = models.CharField(max_length=20)
    imperative = models.CharField(max_length=20, blank=True)

    def __str__(self):
        # plain_ending = Ending.objects.get(pk=self.form).plain
        return self.edict_key




class Verb(models.Model):
    stem = models.CharField(max_length=20)
    group = models.ForeignKey(Group, on_delete=models.PROTECT)
    furigana = models.CharField(max_length=20, blank=True)
    jlpt = models.CharField(max_length=20, blank=True)
    transitive = models.CharField(max_length=20, blank=True)

    def __str__(self):
        if self.group.irregular is False:
            return self.stem + self.group.plain
        elif self.group.edict_key == 'vs':
            return self.stem
        else:
            return self.group.plain

class Module(models.Model):
    title = models.CharField(max_length=20)
    description = models.CharField(max_length=255)
    template_type = models.CharField(max_length=20)
