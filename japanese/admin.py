from django.contrib import admin
from .models import Group, Verb


admin.site.register(Group)
admin.site.register(Verb)
