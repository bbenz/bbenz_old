"use strict";

let verbs = [
  {
    root: "食べ",
    form: "v1"
  },
  {
    root: "見",
    form: "v1"
  },
  {
    root: "遊",
    form: "v5b"
  },
  {
    root: "泳",
    form: "v5g"
  },
  {
    root: "歩",
    form: "v5k"
  },
  {
    root: "読",
    form: "v5m"
  },
  {
    root: "死",
    form: "v5n"
  },
  {
    root: "走",
    form: "v5r"
  },
  {
    root: "話",
    form: "v5s"
  },
  {
    root: "持",
    form: "v5t"
  },
  {
    root: "言",
    form: "v5u"
  },
  {
    root: "行",
    form: "v5k-s"
  },
  {
    root: "あ",
    form: "v5r-i" // ru ending / irregular (basically ある)
  },
  {
    root: "勉強",
    form: "vs"
  },
  {
    root: "来",
    form: "vk"
  },
];

let endings = {
  "v1": {
    'dict': 'る',
    'masu': '',
  },
  "v5b": {
    'dict': 'ぶ',
    'masu': 'び',
  },
  "v5g": {
    'dict': 'ぐ',
    'masu': 'ぎ',
  },
  "v5k": {
    'dict': 'く',
    'masu': 'き',
  },
  "v5m": {
    'dict': 'む',
    'masu': 'み',
  },
  "v5n": {
    'dict': 'ぬ',
    'masu': 'に',
  },
  "v5r": {
    'dict': 'る',
    'masu': 'り',
  },
  "v5s": {
    'dict': 'す',
    'masu': 'し',
  },
  "v5t": {
    'dict': 'つ',
    'masu': 'ち',
  },
  "v5u": {
    'dict': 'う',
    'masu': 'い',
  },
  "v5k-s": {
    'dict': 'く',
    'masu': 'き',
  },
  "v5r-i": {
    'dict': 'る',
    'masu': 'り',
  },
  "vs": {
    'dict': 'する',
    'masu': 'し',
  },
  "vk": {
    'dict': 'る',
    'masu': '',
  },
};

for (let i = 0; i < verbs.length; i++){
  let myEnding = endings[verbs[i].form];
  // console.log(verbs[i].root + myEnding.dict);
}

window.onload = function(){
  // setup the first question
  let masuIncrement = 0;
  let promptDiv = document.querySelector('#prompt');
  let answerDiv = document.querySelector('#answer');
  let answerBtn = document.querySelector('#answerBtn');
  let nextBtn = document.querySelector('#nextBtn');

  function nextQuestion(){
    let myEnding = endings[verbs[masuIncrement].form];
    let dictionary = verbs[masuIncrement].root + myEnding.dict;
    let masuForm = verbs[masuIncrement].root + myEnding.masu + 'ます';
    answerDiv.style.visibility='hidden';
    promptDiv.innerHTML = dictionary;
    answerDiv.innerHTML = masuForm;
    if (masuIncrement !== verbs.length) {
      console.log('yes');
      masuIncrement++;
    } else {
      console.log('bo');
      masuIncrement = 0;
    }
  }

  answerBtn.onclick = function() {
    answerDiv.style.visibility = '';
  }
  nextBtn.onclick = nextQuestion;

  nextQuestion();

}
