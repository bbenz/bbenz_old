from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import Verb

def index(request):
    context = {
        'url_name': 'test',
        'foobar': 'click me!',
        'link_text': 'click here for test!'
    }
    return render(request, 'japanese/index.html', context)

def viewName(request):
    return HttpResponse("you've made it")

