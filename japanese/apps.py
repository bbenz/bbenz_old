from django.apps import AppConfig


class JapaneseConfig(AppConfig):
    name = 'japanese'
